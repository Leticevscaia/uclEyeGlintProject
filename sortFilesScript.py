# -*- coding: utf-8 -*-
"""
This is a script for sorting out the files per each condition
for the 'eye glint study'.

@author:Olga Leticevscaia
"""
import os
from glob import glob
import re
from shutil import copy2

rootDir = 'C:/Users/panda/Documents/ucl/data tomo/';
allFiles = [y for x in os.walk(rootDir) for y in glob(os.path.join(x[0],'*.xlsx'))];
allFilesNum = len(allFiles);

glintAttractNum = 0; noglintAttractNum = 0;
glintTrustNum = 0; noglintTrustNum = 0;

# let's create new directories for our conditions
# glint and noglint with attract/trust in each of them
newDir =r'C:/Users/panda/Documents/ucl/sortedData';

if not os.path.exists(newDir):
    os.makedirs(newDir);
if not os.path.exists(newDir+'/glint/trust'):
    os.makedirs(newDir+'/glint/trust');
if not os.path.exists(newDir+'/noglint/trust'):
    os.makedirs(newDir+'/noglint/trust');
if not os.path.exists(newDir+'/glint/attract'):
    os.makedirs(newDir+'/glint/attract');
if not os.path.exists(newDir+'/noglint/attract'):
    os.makedirs(newDir+'/noglint/attract'); 
               
# walking through each file - counting categories 
# and copying files to category coresponding directories
for fname in allFiles:
    
    fileWExt = os.path.basename(fname);
    name, ext = os.path.splitext(fileWExt);
                                
    if (re.search(r'\_NoGlint',name) != None):
        if (re.search(r'\.1',name) != None): 
            copy2(fname,newDir+'/noglint/attract/'+name+ext);                    
            noglintAttractNum = noglintAttractNum +1;
        else:
            copy2(fname,newDir+'/noglint/trust/'+name+ext);   
            noglintTrustNum = noglintTrustNum +1;
            
    if re.search(r'\_Glint',name) != None:
        if (re.search(r'\.1',name) != None): 
            copy2(fname,newDir+'/glint/attract/'+name+ext);                    
            glintAttractNum = glintAttractNum+1;
        else:
            copy2(fname,newDir+'/glint/trust/'+name+ext);   
            glintTrustNum = glintTrustNum +1;
                 
 # print sums for each category      
print('Total number of files: '+str(allFilesNum)); 
print('Total number of files with Glint: '+
      str(glintAttractNum+glintTrustNum)); 
print('     Attractiveness: '+str(glintAttractNum)+
'     Trustworthiness: '+str(glintTrustNum));
     
print('Total number of files with No Glint: '+
      str(noglintAttractNum+noglintTrustNum)); 
print('     Attractiveness: '+str(noglintAttractNum)+
'     Trustworthiness: '+str(noglintTrustNum));

print('\nTotal number of files rating Attractiveness: '+
      str(noglintAttractNum+glintAttractNum)); 
print('Total number of files rating Trustworthiness: '
      +str(glintTrustNum+noglintTrustNum));    
         
                            



                      