# -*- coding: utf-8 -*-
"""
Created on Thu Jul 12 11:09:53 2018

 Present script is used to read the data (image rating so far)
 from Excel files and calculate the statistics (means, t-test)
 
@author: Olga Leticevscaia

"""
import pandas as pd
import glob 
from scipy.stats import ttest_ind
 
rootDir = r'C:/Users/panda/Documents/ucl/sortedData';
paths = ['/noglint/attract/*.xlsx','/glint/attract/*.xlsx',
        '/noglint/trust/*.xlsx','/glint/trust/*.xlsx'];

rating = {};
for path in paths:
    files = glob.glob(rootDir+path);
    ratingList = [];                  
    for file in files:  
        df = pd.read_excel(file);
        cols = df.columns; 
        ratingList = ratingList + df[cols[0]].values.tolist(); 
                                  
    if path == '/noglint/attract/*.xlsx' :
        rating['noglint_attract'] = ratingList;
    if path == '/glint/attract/*.xlsx' :
        rating['glint_attract'] = ratingList;
    if path == '/noglint/trust/*.xlsx' :
        rating['noglint_trust'] = ratingList;
    if path == '/glint/trust/*.xlsx' :
        rating['glint_trust'] = ratingList;
              
#   mean values          
print('Mean rating of Attractiveness with Glint: '
      +str(sum(rating['glint_attract'])/len(rating['glint_attract']))); 
print('Mean rating of Attractiveness with No Glint: '
      +str(sum(rating['noglint_attract'])/len(rating['noglint_attract'])));  
print('Mean rating of Trustworthiness with Glint: '
      +str(sum(rating['glint_trust'])/len(rating['glint_trust']))); 
print('Mean rating of Trustworthiness with No Glint: '
      +str(sum(rating['noglint_trust'])/len(rating['noglint_trust'])));            
# 2-sample t-tests                                 
print('\n2-sample t-test of Attractiveness:');
print(ttest_ind(rating['glint_attract'], rating['noglint_attract']));
print('\n2-sample t-test of Trustworthiness:');
print(ttest_ind(rating['glint_trust'], rating['noglint_trust']));

