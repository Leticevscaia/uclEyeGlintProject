%{
  glintStudyMANOVA.mat imports the data (ratings from data_olga.xlsx  and
reaction times from data_olga_RTs.xlsl) and runs the MANOVA.
See reference : https://uk.mathworks.com/help/stats/manova.html

author: Olga Leticevscaia
  %}

df = readtable('C:\Users\panda\Documents\ucl\sortedData\data_olga_RTs.xlsx');

 all_groups = [table2array(df(:,1)) table2array(df(:,3)) table2array(df(:,2)) table2array(df(:,4))];
 
 % are there any differences between 4 groups (glint_attract, noglint_attract,
% glint_trust or noglint_trust) ?
 four_groups = [ones(1440,1); 2.* ones(1440,1);3.* ones(1440,1);4.* ones(1440,1)]; 
 [d_four,p_four,stats_four] = manova1(all_groups(:), four_groups(:));
 
 % is there any difference between all glint and all no_glint groups?
  two_groups = [ones(2880,1); 2.* ones(2880,1);];
 [d_two,p_two,stats_two] = manova1(all_groups(:), two_groups(:));

